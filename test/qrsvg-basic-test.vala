class QrsvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/qrsvg/basic",
    ()=>{
      try {
        var qr = new QRSvg.QR ();
        qr.encode ("http://qrsvg", QRSvg.QR.EcQuality.H,
                  QRSvg.QR.CharacterMode.AN,
                  QRSvg.QR.CaseSensitive.NO);
        assert (qr.svg != null);
        message (qr.svg.write_string ());
        if (Config.FILE_OUTPUT) {
          var f = File.new_for_path (Config.TEST_SAVE_DIR+"/basic.svg");
          if (f.query_exists ()) {
            f.delete ();
          }
          qr.svg.write_file (f);
        }
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
