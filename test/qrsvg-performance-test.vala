class QrsvgTest.Suite : Object
{
  static int main (string[] args)
  {
    GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
    Test.init (ref args);
    Test.add_func ("/qrsvg/basic",
    ()=>{
      try {
        var qr = new QRSvg.QR ();
        Test.timer_start ();
        qr.encode ("http://qrsvg.pwmc.mx/1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk1455a87302uiy3847569iwuytr82173jsysk",
                  QRSvg.QR.EcQuality.M,
                  QRSvg.QR.CharacterMode.B8,
                  QRSvg.QR.CaseSensitive.YES);
        message ("Encode time: %g", Test.timer_elapsed ());
        assert (qr.svg != null);
        Test.timer_start ();
        string qrout = qr.svg.write_string ();
        assert (qrout != null);
        message ("Encode time: %g", Test.timer_elapsed ());
      } catch (GLib.Error e) { warning ("Error: "+e.message); }
    });
    return Test.run ();
  }
}
