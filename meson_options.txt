option('introspection', type : 'boolean', value : true, description : 'Build GObject Introspection')
option('docs', type : 'boolean', value : true, description : 'Build Vala Documentation')
option('test-output', type : 'boolean', value : false, description : 'Write file outputs while testing')
