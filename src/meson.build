vapidir = join_paths (get_option('datadir'),'vala','vapi')
GIR_NAME= VERSIONED_CAMEL_CASE_NAME+'.gir'
TYPELIB_NAME= VERSIONED_CAMEL_CASE_NAME+'.typelib'
VAPI_NAME = VERSIONED_PROJECT_NAME+'.vapi'

libqrsvg_deps = [
  dependency('libqrencode', version: '>=4.0.0'),
]

vapi_dirs = []
gir_dirs = []

gxml_dep = dependency('gxml-0.20', version: '>= 0.19.0', required: false)

if not gxml_dep.found ()
  gxml_proy = subproject ('gxml')
  libgxml_build_dir = gxml_proy.get_variable ('libgxml_build_dir')
  gxml_dep = gxml_proy.get_variable ('libgxml_dep')
  vapi_dirs += ['--vapidir', libgxml_build_dir]
  gir_dirs += ['--includedir', libgxml_build_dir]
endif
libqrsvg_deps += gxml_dep

graphene_dep = dependency('graphene-1.0', version:'>=1.8.2', fallback: ['graphene', 'graphene_dep'])
libqrsvg_deps += graphene_dep

gsvg_dep = dependency('gsvg-0.8', version: '>= 0.7.0', required: false)


if not gsvg_dep.found ()
  gsvg_proy = subproject ('gsvg')
  libgsvg_build_dir = gsvg_proy.get_variable ('libgsvg_build_dir')
  gsvg_dep = gsvg_proy.get_variable ('libgsvg_dep')
  vapi_dirs += ['--vapidir', libgsvg_build_dir]
  gir_dirs += ['--includedir', libgsvg_build_dir]
endif
libqrsvg_deps += gsvg_dep

conf = configuration_data()
conf.set('PROJECT_NAME', PROJECT_NAME)
conf.set('CAMEL_CASE_NAME', CAMEL_CASE_NAME)
conf.set('API_VERSION', API_VERSION)

nsinfo = configure_file(input : 'namespace-info.vala.in',
	output : 'namespace-info.vala',
	configuration : conf)
namespaceinfo_dep = declare_dependency (sources : nsinfo)

configure_file(input : 'qrsvg.deps.in',
	output : PROJECT_NAME+'-@0@.deps'.format(API_VERSION),
	configuration : conf,
	install : true,
	install_dir : join_paths(get_option('datadir'), 'vala', 'vapi')
)

valasources = files([
  'qrsvg-qr.vala'
])


inc_libh = include_directories ('.')
inc_libh_dep = declare_dependency (include_directories : inc_libh)
libqrsvg_src_dir = meson.current_source_dir ()
libqrsvg_build_dir = meson.current_build_dir ()

qrencodvapi = files(['libqrencode.vapi'])
install_data(qrencodvapi, install_dir: vapidir)
qrencodevapi_dir = join_paths(meson.current_source_dir ())

# LT_VERSION for ABI related changes
# From: https://autotools.io/libtool/version.html
# This rules applies to Meson 0.43
# Increase the current value whenever an interface has been added, removed or changed.
# Always increase revision value whenever an interface has been added, removed or changed.
# Increase the age value only if the changes made to the ABI are backward compatible.
# Set version to the value of subtract age from current
# Reset current and version to 1 and, age and version to 0 if library's name is changed
LT_CURRENT='1'
LT_REVISION='0'
LT_AGE='0'
LT_VERSION='1'
libqrsvg = library(VERSIONED_PROJECT_NAME,
	valasources,
	version : LT_VERSION,
	soversion : LT_VERSION+'.'+LT_AGE+'.'+LT_REVISION,
	vala_header : PROJECT_NAME+'.h',
	vala_vapi : VAPI_NAME,
	vala_gir : GIR_NAME,
	c_args: ['-I'+join_paths (get_option('prefix'),get_option('includedir'))],
	vala_args: [
		'--target-glib=2.50',
		'--vapidir='+qrencodevapi_dir
		],
	dependencies: [libqrsvg_deps, namespaceinfo_dep],
	install: true,
	install_dir : [
		get_option('libdir'),
		join_paths (get_option('includedir'),VERSIONED_PROJECT_NAME),
		vapidir,
		true
	]
)


libqrsvg_build_dir=meson.current_build_dir()
libqrsvg_dep = declare_dependency(
	include_directories : inc_libh,
 	link_with : libqrsvg,
 	dependencies: libqrsvg_deps
 	)

if get_option('introspection')
g_ir_compiler = find_program('g-ir-compiler')
custom_target('typelib',
	command: [
		g_ir_compiler,
		'--shared-library', 'lib'+PROJECT_NAME+'-@0@.so'.format (API_VERSION),
		gir_dirs,
		'--output', '@OUTPUT@',
		join_paths(meson.current_build_dir(), GIR_NAME)
	],
	output: TYPELIB_NAME,
	depends: libqrsvg,
	install: true,
	install_dir: join_paths(get_option('libdir'), 'girepository-1.0'))
endif
