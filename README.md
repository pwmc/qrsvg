QRsvg is library to create QR code using SVG files.

It requires:

* libqrencode
* GSVG - https://gitlab.com/pwmc/gsvg
